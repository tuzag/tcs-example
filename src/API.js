let baseURL =
	process.env.REACT_APP_ENV === "PROD"
		? `https://apistaging.tuzagtcs.com` //your endpoint URL here if self hosting
		: `http://localhost:3000`;

const API = async (endpoint, method, data = {}, forceLocal = false) => {
	let options = {};

	if (forceLocal) {
		baseURL = `http://localhost:3000`;
	}

	if (method !== "GET") {
		if (!data.userData) {
			data.userData = {};
		}
		options.body = JSON.stringify({
			...data,
			includeFormatting: true,
			userData: data.userData,
		});
	}
	const rawResult = await fetch(`${baseURL}${endpoint}`, {
		...options,
		method,
		headers: {
			"Content-Type": "application/json",
			apiKey: "[[API KEY HERE]]",
		},
	});

	const result = await rawResult.json();

	if (result.error) {
		alert(result.error);
		return result;
	} else {
		return result;
	}
};

export default API;
