const config = {
	auth: {
		userAuth: false,
		bcrypt: "$2a$12$k6P20X1k10R0rxx7rIiWy.RGmCY19ABMLQr0vUSvAOSOhy9nFQ2cC",
	},
	projectID: "EiHQvokkQXCL5S3yw",
};

export default config;
