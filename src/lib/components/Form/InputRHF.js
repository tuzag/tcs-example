import React from "react";
import "./input.scss";

const InputRHF = ({ name, label = "", register, error = {}, ...props }) => {
	return (
		<label className={error[name] ? "error" : ""}>
			{label && <span className="label">{label}</span>}
			<input {...register(name)} {...props} />
			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
};

export default InputRHF;
