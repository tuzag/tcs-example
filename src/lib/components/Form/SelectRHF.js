import React from "react";
import Select, { components } from "react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SelectRHF = ({
	name,
	label = "",
	options,
	error = {},
	isClearable = false,
	...props
}) => {
	const customStyles = {
		control: (provided, state) => {
			let controlBGColor = "var(--white)";
			let controlBorder = "var(--border-size) solid var(--black)";

			if (error[name]) {
				controlBorder = "var(--border-size) solid var(--red)";
			}

			return {
				...provided,
				width: "100%",
				display: "flex",
				padding: "12px 15px 11px 15px",
				backgroundColor: controlBGColor,
				border: controlBorder,
				borderRadius: 0,
				boxShadow: "none",
				fontSize: "var(--font-size-body)",
				color: "var(--black)",
				":hover": {
					border: controlBorder,
				},
			};
		},
		valueContainer: () => ({
			width: `calc(100% - ${isClearable ? 52 : 28}px)`,
			position: "relative",
			padding: 0,
		}),
		input: () => ({
			margin: 0,
			padding: 0,
			lineHeight: "var(--line-height-condensed)",
		}),
		indicatorsContainer: () => ({
			display: "flex",
			alignItems: "center",
		}),
		indicatorSeparator: () => ({
			display: "none",
		}),
		clearIndicator: (provided) => ({
			...provided,
			padding: "0 10px 0 0",
			color: "var(--black)",
			":hover": {
				color: "var(--black)",
			},
		}),
		dropdownIndicator: () => ({
			display: "flex",
		}),
		menu: (provided) => ({
			...provided,
			margin: "-2px 0 0 0",
			border: "var(--border-size) solid var(--black)",
			borderRadius: 0,
			boxShadow: 0,
		}),
		menuPortal: (provided) => ({
			...provided,
			zIndex: 99999,
		}),
		menuList: (provided) => ({
			...provided,
			padding: 0,
		}),
		option: (provided, state) => {
			let optionBGColor = "var(--white)";
			let optionColor = "var(--black)";

			if (state.isSelected) {
				optionBGColor = "var(--black)";
				optionColor = "var(--white)";
			}

			return {
				...provided,
				padding: "10px 25px",
				backgroundColor: optionBGColor,
				fontSize: "var(--font-size-body)",
				color: optionColor,
				lineHeight: "var(--line-height-body)",
				":hover": {
					backgroundColor: "var(--black)",
					color: "var(--white)",
				},
			};
		},
		singleValue: (provided, state) => ({
			...provided,
			maxWidth: "100%",
			position: "absolute",
			margin: 0,
			color: "inherit",
		}),
	};

	const DropdownIndicator = (props) => {
		return (
			<components.DropdownIndicator {...props}>
				<FontAwesomeIcon
					icon={props.selectProps.menuIsOpen ? "chevron-up" : "chevron-down"}
					style={{ fontSize: "var(--font-size-supporting)" }}
				/>
			</components.DropdownIndicator>
		);
	};

	return (
		<label className={error[name] ? "error" : ""}>
			{label && <span className="label">{label}</span>}
			<Select
				components={{ DropdownIndicator }}
				options={options}
				placeholder=""
				menuPlacement="auto"
				styles={customStyles}
				name={name}
				menuPortalTarget={document.body}
				isClearable={isClearable}
				{...props}
			/>

			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
};

export default SelectRHF;
