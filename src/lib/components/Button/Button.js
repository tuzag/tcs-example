import React from "react";
import "./Button.scss";

const Button = ({ addClass, text, style = {}, ...props }) => {
	return (
		<button
			style={{ ...style }}
			className={`button ${addClass ? addClass : ""}`}
			{...props}
		>
			<span>{text}</span>
		</button>
	);
};

export default Button;
