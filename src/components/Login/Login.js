import React from "react";

import config from "../../config/config";

import LoginForm from "./LoginForm/LoginForm";

const Login = () => {
	return (
		<div className="login">
			<div className="login-inner">
				<div className="login-content">
					<LoginForm config={config} />
				</div>
			</div>
		</div>
	);
};

export default Login;
