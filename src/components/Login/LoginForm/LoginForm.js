import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import bcrypt from "bcryptjs";
import { nanoid } from "nanoid";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import InputRHF from "../../../lib/components/Form/InputRHF";
import SelectRHF from "../../../lib/components/Form/SelectRHF";
import Button from "../../../lib/components/Button/Button";

import "../Login.scss";

const LoginForm = ({ config }) => {
	const [users, setUsers] = useState([]);
	const [user, setUser] = useState("");
	const [password, setPassword] = useState("");
	const [error, setError] = useState("");

	const navigate = useNavigate();

	let schema;

	if (config.auth.userAuth) {
		schema = yup.object().shape({
			user: yup.object().required("Please select a user."),
			password: yup.string().required("Please enter a password."),
		});
	} else {
		schema = yup.object().shape({
			password: yup.string().required("Please enter a password."),
		});
	}

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
		setValue,
	} = useForm({
		resolver: yupResolver(schema),
	});

	useEffect(() => {
		if (config.auth.userAuth) {
			(async () => {
				const res = await fetch(config.auth.userData);
				const data = await res.json();
				setUsers(
					data.map((user) => ({ value: user["UserID"], label: user[" "] }))
				);
			})();
		}
	});

	useEffect(() => {
		if (user) {
			setValue("user", user);
		}
	}, [user]);

	const onSubmit = async (data) => {
		const passwordMatch = await bcrypt.compare(password, config.auth.bcrypt);

		if (passwordMatch) {
			window.localStorage.setItem(
				"userID",
				config.auth.userAuth ? user : nanoid()
			);
			window.localStorage.setItem("auth", true);
			navigate(`/chat`);
		} else {
			setError(
				"Invalid password. Please enter a different password and try again."
			);
		}
	};

	return (
		<div className="login-form">
			<div className="intro">
				<h1 className="h2">
					<em>tuzagTCS</em> Example
				</h1>
				<p>Welcome! Please sign in.</p>
			</div>
			<form onSubmit={(event) => event.preventDefault()}>
				{config.auth.userAuth && (
					<div className="grid-row">
						<div className="grid-col-full">
							<Controller
								name="user"
								control={control}
								render={({ field }) => (
									<SelectRHF
										name={field.name}
										label="User"
										options={users || [{ value: 0, label: "Loading..." }]}
										{...field}
										ref={null}
										error={errors}
										onChange={(value) => {
											setUser(value);
										}}
									/>
								)}
							/>
						</div>
					</div>
				)}
				<div className="grid-row last">
					<div className="grid-col-full">
						<InputRHF
							name="password"
							label="Password"
							type="password"
							autoComplete="current-password"
							value={password}
							onChange={(evt) => {
								setPassword(evt.target.value);
							}}
							register={register}
							error={errors}
						/>
					</div>
				</div>
				{error && <p className="form-response error global">{error}</p>}
			</form>
			<div className="button-wrap">
				<div className="button-wrap-inner">
					<Button
						text="Sign In"
						type="submit"
						addClass="no-margin-bottom"
						onClick={handleSubmit(onSubmit)}
					/>
					<div className="version">v1.0.0</div>
				</div>
			</div>
		</div>
	);
};

export default LoginForm;
