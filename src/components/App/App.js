import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { library } from "@fortawesome/fontawesome-svg-core";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import "focus-visible/dist/focus-visible.js";

import Login from "../Login/Login";
import Chat from "../Chat/Chat";

import "normalize.css";
import "../../lib/components/Form/forms.scss";
import "../../lib/styles/grid.scss";
import "../../lib/styles/typography.scss";
import "./App.scss";

library.add(far, fas);

function App() {
	useEffect(() => {
		const docHeight = () => {
			document.documentElement.style.setProperty(
				"--doc-height",
				`${window.innerHeight}px`
			);
		};

		window.addEventListener("resize", docHeight);
		docHeight();
	}, []);

	return (
		<BrowserRouter>
			<RoutesContainer />
		</BrowserRouter>
	);
}

const RoutesContainer = () => {
	return (
		<Routes>
			<Route path="/" element={<Login />} />
			<Route path="/chat" element={<Chat />} />
		</Routes>
	);
};

export default App;
