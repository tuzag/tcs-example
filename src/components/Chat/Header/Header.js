import React from "react";
import "./Header.scss";

const Header = ({ resetChat }) => {
	return (
		<header className="header">
			<div className="header-inner">
				<div className="page-title">
					<h1 className="h3">
						<em>tuzagTCS</em> Example
					</h1>
				</div>
				{resetChat && (
					<button className="link" onClick={resetChat}>
						(<span>start over</span>)
					</button>
				)}
			</div>
		</header>
	);
};

export default Header;
