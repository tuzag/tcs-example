import React, { useState, useEffect } from "react";
import {
	Widget,
	toggleWidget,
	renderCustomComponent,
	isWidgetOpened,
	setQuickButtons,
	deleteMessages,
	toggleInputDisabled,
	addUserMessage,
} from "react-chat-widget";
import { useForm } from "react-hook-form";
import striptags from "striptags";

import ContentAPI from "../../../lib/ContentAPI";

import Loading from "../Loading/Loading";
import SelectRHF from "../../../lib/components/Form/SelectRHF";
import CheckboxRHF from "../../../lib/components/Form/CheckboxRHF";
import Button from "../../../lib/components/Button/Button";

import "react-chat-widget/lib/styles.css";
import "../Chat.scss";
import "./ChatBox.scss";

let session;
let userData = {};
let messages = [];

const ChatBox = ({ config, resetChat }) => {
	const [statementMode, setStatementMode] = useState(false);
	const [loading, setLoading] = useState(true);
	const [statement, setStatement] = useState(false);
	const [shouldEndSession, setShouldEndSession] = useState(false);
	const [otherMode, setOtherMode] = useState(false);

	const userID = window.localStorage.getItem("userID");

	const {
		register,
		formState: { errors },
	} = useForm();

	const enableInput = () => {
		if (document.getElementsByClassName("rcw-message-disable").length) {
			toggleInputDisabled();
		}
	};

	const disableInput = () => {
		if (document.getElementsByClassName("rcw-message-disable").length === 0) {
			toggleInputDisabled();
		}
	};

	useEffect(() => {
		if (!window.location.href.includes("cache")) {
			window.location.href = `${window.location.href}?cache=${Math.round(
				Math.random() * 10000
			)}`;
		} else if (!isWidgetOpened()) {
			toggleWidget();

			renderCustomComponent(
				typingMessage,
				{
					date: Date.now(),
					type: "concierge",
				},
				false,
				"loading"
			);
		}
	}, []);

	useEffect(() => {
		(async () => {
			let sessionStatus = await ContentAPI(
				`/conversation/session/status/${userID}`,
				"GET"
			);

			console.log(sessionStatus);

			if (!sessionStatus.sessionInProgress) {
				sessionStatus = await ContentAPI(
					`/conversation/session/start`,
					"POST",
					{
						userID,
						projectID: config.projectID,
					}
				);

				if (sessionStatus.error) {
					alert("Error, something went wrong");
					console.error(sessionStatus.error);
				}
			}
			session = sessionStatus.sessionDetails;
			deleteMessages();
			for (const [index, message] of session.messages.entries()) {
				if (message.sentByUser) {
					messages.push({ type: "user", content: message.content });
					addUserMessage(message.content, message._id);
					renderCustomComponent(meta, {
						date: new Date(message.timestamp),
						type: "user",
					});
				} else {
					if (message.checkbox) {
						renderCustomComponent(checkboxOptionsMessage, {
							message,
							options: message.options,
						});
					} else if (message.survey) {
						renderCustomComponent(surveyOptionsMessage, {
							options: message.options,
						});
					} else {
						messages.push({
							type: "concierge",
							content: striptags(message.content),
						});
						renderCustomComponent(conciergeMessage, {
							message: message.content,
							date: new Date(message.timestamp),
						});

						if (
							index + 1 === session.messages.length ||
							session.messages[index + 1]?.sentByUser
						) {
							renderCustomComponent(meta, {
								date: new Date(message.timestamp),
								type: "concierge",
							});
						}
					}
				}
			}

			setLoading(false);
		})();
	}, []);

	useEffect(() => {
		if (!isWidgetOpened()) {
			toggleWidget();
		}
		renderCustomComponent(
			typingMessage,
			{
				date: Date.now(),
				type: "concierge",
			},
			false,
			"loading"
		);
	}, []);

	useEffect(() => {
		if (statementMode) {
			setQuickButtons([
				{
					label: statement
						? statement.replace("<p>", "").replace("</p>", "")
						: "Continue",
					value: "continue",
				},
			]);
		} else {
			setQuickButtons([]);
		}
	}, [statementMode]);

	const randInt = (min, max) => {
		return Math.floor(Math.random() * (max - min + 1) + min);
	};

	const meta = ({ date, type }) => {
		const timestamp = new Intl.DateTimeFormat("en-US", {
			hour: "numeric",
			minute: "2-digit",
		}).format(date);

		return (
			<div className={`rcw-meta ${type === "user" && "rcw-client"}`}>
				<div className="rcw-timestamp">{timestamp}</div>
			</div>
		);
	};

	const conciergeMessage = ({ message }) => {
		const createMarkup = () => ({
			__html: message,
		});

		return (
			<div className="rcw-response">
				<div className="rcw-message-text">
					<p>
						<span dangerouslySetInnerHTML={createMarkup()} />
					</p>
				</div>
			</div>
		);
	};

	const surveyOptionsMessage = ({ options }) => {
		const formattedOptions = (options) => {
			if (options) {
				const handleSelection = (option) => {
					addUserMessage(option.label);

					let flag = null;

					if (option.value === "Other" || option.name === "Other") {
						const needsUserResponse = true;
						const responseRequest = "Please tell me more about that.";
						const responseFollowUp = "Thanks! Let's move on.";

						flag = {
							type: "other",
							needsUserResponse,
							responseRequest,
							responseFollowUp,
						};

						if (needsUserResponse) {
							setOtherMode(true);
						}
					}

					handleNewUserMessage(option.label, option.value || option.name, flag);
				};

				if (options.length > 8) {
					return (
						<SelectRHF
							name="options"
							options={options.map((option) => ({
								value: option.name,
								label: option.label,
							}))}
							ref={null}
							error={errors}
							onChange={(value, fieldData) => {
								handleSelection(value);
							}}
						/>
					);
				} else {
					return options.map((option) => (
						<Button
							key={option.name}
							text={option.label}
							onClick={(event) => {
								event.preventDefault();
								handleSelection(option);
							}}
						/>
					));
				}
			}
		};

		return (
			<div className="rcw-response">
				<div className="rcw-message-text options">
					<div className="rcw-message-options">{formattedOptions(options)}</div>
				</div>
			</div>
		);
	};

	const checkboxOptionsMessage = ({ options, message }) => {
		const continueClicked = (evt) => {
			evt.preventDefault();

			let checked = [];
			for (const element of evt.target.elements) {
				if (element.checked) {
					checked.push(element.name);
				}
			}
			enableInput();
			handleNewUserMessage(null, null, null, checked);
		};

		return (
			<div className={`rcw-response`}>
				<div className={`rcw-message-text`}>
					<form onSubmit={continueClicked}>
						<div className="checkbox-group">
							{options.map((option, index) => (
								<div key={message._id + option.name + index}>
									<CheckboxRHF
										name={option.name}
										label={option.label}
										register={register}
										error={errors}
									/>
								</div>
							))}
							<Button text="Continue" type="submit" />
						</div>
					</form>
				</div>
			</div>
		);
	};

	const typingMessage = () => (
		<div className="rcw-response">
			<div className="rcw-message-text loading-message">
				<Loading />
			</div>
		</div>
	);

	const handleNewUserMessage = async (
		message,
		optionName,
		userMsgFlag = null,
		selectedCheckboxes
	) => {
		const typingDelay = true;
		const sendTime = new Date();
		if (typingDelay) {
			disableInput();
		}

		if (!statementMode && !selectedCheckboxes) {
			renderCustomComponent(meta, {
				date: Date.now(),
				type: "user",
			});
		}

		if (!selectedCheckboxes) {
			messages.push({
				type: "user",
				content: message,
			});
		}

		renderCustomComponent(
			typingMessage,
			{
				date: Date.now(),
				type: "concierge",
			},
			false,
			"loading"
		);

		const data = await ContentAPI(`/conversation/message`, "POST", {
			sessionID: session._id,
			message: optionName ? optionName : message,
			selectedCheckboxes,
		});

		console.log(data);

		userData = { ...userData, ...data.newUserData };

		let waitTimes = [];

		if (otherMode) {
			setTimeout(
				() => {
					deleteMessages(1, "loading");

					if (message.checkbox) {
						renderCustomComponent(checkboxOptionsMessage, {
							message,
							options: message.options,
						});
					} else if (message.survey) {
						renderCustomComponent(surveyOptionsMessage, {
							options: message.options,
						});
					} else {
						messages.push({
							type: "concierge",
							content: userMsgFlag?.responseRequest,
						});
						renderCustomComponent(conciergeMessage, {
							message: userMsgFlag?.responseRequest,
						});
						renderCustomComponent(meta, {
							date: Date.now(),
							type: "concierge",
						});
						handleUIChangesAfterMessageShown();
					}
				},
				typingDelay ? 1500 : 0
			);

			setOtherMode(false);
		} else if (
			userMsgFlag?.type &&
			userMsgFlag.type === "other" &&
			userMsgFlag?.needsUserResponse &&
			userMsgFlag.needsUserResponse === true
		) {
			setTimeout(
				() => {
					deleteMessages(1, "loading");

					messages.push({
						type: "concierge",
						content: userMsgFlag?.responseRequest,
					});
					renderCustomComponent(conciergeMessage, {
						message: userMsgFlag?.responseRequest,
					});
					renderCustomComponent(meta, {
						date: Date.now(),
						type: "concierge",
					});
					handleUIChangesAfterMessageShown();
				},
				typingDelay ? 1500 : 0
			);
		} else {
			if (!typingDelay) {
				for (let i = 0; i < data.newMessages.length; i++) {
					waitTimes.push(250 * i + 1);
				}
			} else {
				for (const message of data.newMessages) {
					if (message.survey || message.checkbox) {
						waitTimes.push(waitTimes[waitTimes.length - 1] + 10);
					} else {
						waitTimes.push(
							Math.max(
								1500,
								Math.max(
									message.content.length * randInt(10, 14) -
										Math.abs(new Date() - sendTime),
									0
								),
								0
							) + (waitTimes.length === 0 ? 0 : waitTimes[waitTimes.length - 1])
						);
					}
				}
			}

			for (const [index, message] of data.newMessages.entries()) {
				setTimeout(() => {
					deleteMessages(1, "loading");

					if (message.checkbox) {
						renderCustomComponent(checkboxOptionsMessage, {
							message,
							options: message.options,
						});
					} else if (message.survey) {
						renderCustomComponent(surveyOptionsMessage, {
							options: message.options,
						});
					} else {
						messages.push({
							type: "concierge",
							content: striptags(message.content),
						});
						renderCustomComponent(conciergeMessage, {
							message: message.content,
						});
					}

					if (index === data.newMessages.length - 1) {
						renderCustomComponent(meta, {
							date: Date.now(),
							type: "concierge",
						});
						handleUIChangesAfterMessageShown();
					} else {
						renderCustomComponent(
							typingMessage,
							{
								date: Date.now(),
								type: "concierge",
							},
							false,
							"loading"
						);
					}
				}, waitTimes[index]);
			}
		}

		const handleUIChangesAfterMessageShown = () => {
			if (data.newMessages[data.newMessages.length - 1]?.checkbox) {
				disableInput();
			} else {
				setStatement(
					data.messageType === "statement" ? data.statementButton : null
				);
				setStatementMode(data.messageType === "statement");
				setShouldEndSession(data.session?.sessionCompleted);
				enableInput();
				console.log(data.session);
				document.getElementsByClassName("rcw-input")[0].focus();
				if (data.session?.sessionCompleted) {
					disableInput();
					setQuickButtons([
						{
							label: "Start Over",
							value: "Start Over",
						},
					]);
				}
			}
		};
	};

	const handleQuickButtonClicked = async () => {
		if (shouldEndSession) {
			resetChat();
		} else {
			await handleNewUserMessage("");
			setStatementMode(false);
		}
	};

	return (
		<div className="chat-box">
			<div className="chat-box-inner">
				<div
					className={`interaction ${
						statementMode === true ? "statement" : ""
					} ${shouldEndSession === true ? "end" : ""}`}
				>
					<div className="interaction-inner">
						<Widget
							handleNewUserMessage={handleNewUserMessage}
							handleQuickButtonClicked={handleQuickButtonClicked}
							fullScreenMode={true}
							title=""
							subtitle=""
							showTimeStamp={false}
						/>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ChatBox;
