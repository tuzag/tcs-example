import React from "react";
import { nanoid } from "nanoid";

import config from "../../config/config";

import Header from "./Header/Header";
import ChatBox from "./ChatBox/ChatBox";

import "./Chat.scss";

const Chat = () => {
	const resetChat = () => {
		window.localStorage.setItem("userID", nanoid());
		window.location.reload();
	};

	return (
		<div className="chat">
			<div className="chat-inner">
				<div className="chat-content">
					<div className="chat-content-inner">
						<Header resetChat={resetChat} />
						<ChatBox config={config} resetChat={resetChat} />
					</div>
				</div>
			</div>
		</div>
	);
};

export default Chat;
