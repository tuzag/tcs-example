module.exports = function override(config, env) {
	config["resolve"] = {
		fallback: { crypto: false, buffer: false, stream: false },
	};
	return config;
};
